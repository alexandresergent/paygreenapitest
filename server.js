const express = require('express')
const app = express()
const { Transaction } = require('./models/')

app.use(express.json());

app.get('/', function (req, res) {
    res.send('PayGreenApiTest!')
})

app.post('/transaction', function (req, res) { // generate a transaction

    if (isNaN(req.body.idPsp) || req.body.idPsp.length === 0) {
        res.status(400).json({ error: 'invalid idPsp' });
    } else if (isNaN(req.body.idPayGreen) || req.body.idPayGreen.length === 0) {
        res.status(400).json({ error: 'invalid idPayGreen' });
    }else if (req.body.href.trim().length === 0) {
        res.status(400).json({ error: 'invalid href' });
    } else {
        Transaction.create({ idPsp: req.body.idPsp, idPayGreen: req.body.idPayGreen, state: 'AWAITING', href: req.body.href }).then(transaction => {
            res.status(201).json({ id: transaction.dataValues.id });
        })
    }
})

app.get('/transaction/:id', function (req, res, next) { // get the status of a transaction

    if (isNaN(req.params.id)) {
        res.status(400).json({ error: 'invalid identifier' });
    } else {

        Transaction.findOne({
            where: {
                id: req.params.id
             }
        }).then(transaction => {
            const transactionResult = transaction ? { transaction: transaction.dataValues } : {};
            res.status(200).json(transactionResult);
        })
    }
})

app.post('/refund', async function (req, res) { // refund a transaction

    if (isNaN(req.body.idTransaction)) {
        res.status(400).json({ error: 'invalid idTransaction' });
    } else {
        Transaction.update(
            {state: 'REFUND'},
            {returning: true, where: {id: req.body.idTransaction} }
        ).then(transaction => {
            res.status(200).json({ transaction: transaction[1][0].dataValues });
        })
    }
})

app.listen(3000, function () {
    console.log('PayGreenApitest is ready on port 3000!')
})

module.exports = app;
