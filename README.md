# PayGreen Api Test

## Prérequis: nodejs, npm, docker

### 1. Start a postgres instance via docker
```
docker pull postgres
docker run --name postgres_server -e POSTGRES_PASSWORD=test -d -p 5432:5432 postgres
```

### 2. Connect to docker container
```
docker exec -it postgres_server bash
```

### 3. Create a paygreen database
```
psql -U postgres
CREATE DATABASE paygreen;
exit
exit
```

### 4. Install modules
```
npm install
```

### 5. Migrate database
```
node_modules/.bin/sequelize db:migrate
```

### 6. Run test
```
npm run test
```

### 7. Run server
```
node server.js
```
