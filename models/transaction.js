'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    idPsp: DataTypes.INTEGER,
    idPayGreen: DataTypes.INTEGER,
    state: DataTypes.STRING,
    href: DataTypes.STRING
  }, {});
  Transaction.associate = function(models) {
    // associations can be defined here
  };
  return Transaction;
};