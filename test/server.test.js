const assert = require('assert');
const request = require('supertest');
const server = require('../server')
const { Transaction } = require('../models/')

describe('Unit testing generate a transaction', function() {

    it('should return transaction error idPsp', function() {
        return request(server)
            .post('/transaction')
            .send({
                'idPsp': 'abc1',
                'idPayGreen': 1,
                'href': 'https://secure.payment.com'
            })
            .then(function(response){
                assert.equal(response.status, 400)
                assert.equal(response.body.error, 'invalid idPsp')
            })
    });

    it('should return transaction error idPayGreen', function() {
        return request(server)
            .post('/transaction')
            .send({
                'idPsp': 1,
                'idPayGreen': '1abc',
                'href': 'https://secure.payment.com'
            })
            .then(function(response){
                assert.equal(response.status, 400)
                assert.equal(response.body.error, 'invalid idPayGreen')
            })
    });

    it('should return transaction error href', function() {
        return request(server)
            .post('/transaction')
            .send({
                'idPsp': 1,
                'idPayGreen': 1,
                'href': ' '
            })
            .then(function(response){
                assert.equal(response.status, 400)
                assert.equal(response.body.error, 'invalid href')
            })
    });

    it('should return transaction created', function() {
        return request(server)
            .post('/transaction')
            .send({
                'idPsp': 1,
                'idPayGreen': 1,
                'href': 'https://secure.payment.com'
            })
            .then(function(response){
                assert.equal(response.status, 201)
                assert.notEqual(response.body.id, null)
            })
    });
});

describe('Unit testing get the status of a transaction', function() {

    it('should return transaction error identifier', function() {
        return request(server)
            .get('/transaction/abc')
            .then(function(response){
                assert.equal(response.status, 400)
                assert.equal(response.body.error, 'invalid identifier')
            })
    });

    it('should return {}', function() {
        return request(server)
            .get('/transaction/999')
            .then(function(response){
                assert.equal(response.status, 200)
                assert.deepEqual(response.body, {})
            })
    });

    it('should return transaction state', async function() {
        const transaction = await Transaction.findOne({
            order: [ [ 'createdAt', 'DESC' ]],
        });
        return request(server)
            .get('/transaction/' + transaction.dataValues.id)
            .then(function(response){
                assert.equal(response.status, 200)
                assert.equal(response.body.transaction.id, transaction.dataValues.id)
                assert.equal(response.body.transaction.idPsp, 1)
                assert.equal(response.body.transaction.idPayGreen, 1)
                assert.equal(response.body.transaction.state, 'AWAITING')
                assert.equal(response.body.transaction.href, 'https://secure.payment.com')
            })
    });
});

describe('Unit testing refund a transaction', function() {

    it('should return refund error idTransaction', function() {
        return request(server)
            .post('/refund')
            .send({
                'idTransaction': 'abc1',
            })
            .then(function(response){
                assert.equal(response.status, 400)
                assert.equal(response.body.error, 'invalid idTransaction')
            })
    });

    it('should return transaction with state REFUND', async function() {
        const transaction = await Transaction.findOne({
            order: [ [ 'createdAt', 'DESC' ]],
        });
        return request(server)
            .post('/refund')
            .send({
                'idTransaction': transaction.dataValues.id,
            })
            .then(function(response){
                assert.equal(response.status, 200)
                assert.equal(response.body.transaction.state, 'REFUND')
            })
    });
});
